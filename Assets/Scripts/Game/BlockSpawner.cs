﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    public GameObject Player;
    public GameObject [] LevelBlocks;
    public float [] BlockLengths;
    private float Currentlength = 0;
    private int blockIndex;
    private int lastBlock;
    private Vector3 newSpawnPoistion;
    // Start is called before the first frame update
    void Start()
    {
        NewBlock();
    }
    void NewBlock()
    {
        blockIndex = Random.Range(0,LevelBlocks.Length);
        if (blockIndex == lastBlock)
        {
            blockIndex = Random.Range(0,LevelBlocks.Length);
        }
        else
        {
            lastBlock = blockIndex;
        }
        newSpawnPoistion = new Vector3(0,0,Currentlength);
        Instantiate(LevelBlocks[blockIndex], newSpawnPoistion, Quaternion.identity);
        Currentlength += 30;
    }
    // Update is called once per frame
    void Update()
    {
        if (Currentlength-30 <= Player.transform.position.z)
        {
            NewBlock();
        }
    }
}
