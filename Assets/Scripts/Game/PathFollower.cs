﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PathFollower : MonoBehaviour
{
    //array of all the nodes
    public List<Node> PathNode;
    //array of all the enemys
    public GameObject [] Enemy;
    //moving the enemies around the node array
    public float MoveSpeed = 0.5f;
    float Timer;
    //variables for the spawning of the node
    public float SpawnDelay = 1.5f;
    public float rotateSpeed = 1.0f;
    //current node to travel to
    int CurrentNode;
    static public Vector3 CurrentPositionHolder;
    
    public GameObject Player;
    //node object to spawn
    public GameObject PrefabNode;

    public GameObject light1;
    private Light light1Comp;
    public GameObject light2;
    private Light light2Comp;
    public GameObject light3;
    private Light light3Comp;

    private float PlayTime;
    // Start is called before the first frame update
    void Start()
    {   
        Enemy = GameObject.FindGameObjectsWithTag("Enemy");
        PathNode = new List<Node>();
        Invoke("SpawnNode", SpawnDelay);
        light1Comp = light1.GetComponent<Light>();
        light2Comp = light2.GetComponent<Light>();
        light3Comp = light3.GetComponent<Light>();
    }

    void CheckNode()
    {
        if(CurrentNode < PathNode.Count-1)
        {
            Timer = 0;
            CurrentPositionHolder = PathNode[CurrentNode].transform.position;
        }
    }
    void DrawLine()
    {
        for (int i = 0; i < PathNode.Count; i++)
        {
            if (i<PathNode.Count - 1)
            {
                Debug.DrawLine (PathNode [i].transform.position, PathNode[i+1].transform.position, Color.green);
            }
        }
    }

    void SpawnNode()
    {
        PathNode.Add(Instantiate(PrefabNode, Player.transform.position, Player.transform.rotation).GetComponent<Node>());
        Invoke("SpawnNode", SpawnDelay);
    }
    // Update is called once per frame
    void OnCollisionEnter(Collision collision){
         if (collision.gameObject.tag == "Player"){
            gameObject.transform.position = Vector3.zero;
            gameObject.GetComponent<Renderer>().enabled = false;
            light1Comp.enabled = false;
            light2Comp.enabled = false;
            light3Comp.enabled = false;
            CurrentPositionHolder = Vector3.zero;
            foreach (Node position in PathNode)
            {
                position.transform.position = Vector3.zero;
            }
            PlayTime = 0;
            Score.SaveScoreToFile();
            Score.LoadFromFile();
            SceneManager.LoadScene(0);
            print("player is ded");
         }
    }

    private void FixedUpdate()
    {
        
    }
    void Update()
    {
        PlayTime += Time.deltaTime;

        if (PlayTime >= 10)
        {
            gameObject.GetComponent<Renderer>().enabled = true;
            light1Comp.enabled = true;
            light2Comp.enabled = true;
            light3Comp.enabled = true;
            CheckNode();
            DrawLine();
            Timer += Time.deltaTime * MoveSpeed;
            foreach (GameObject enemy in Enemy)
            {   
                Vector3 targetDirection = Player.transform.position - enemy.transform.position;
                // The step size is equal to speed times frame time.
                float singleStep = rotateSpeed * Time.deltaTime;
                // Rotate the forward vector towards the target direction by one step
                Vector3 newDirection = Vector3.RotateTowards(enemy.transform.forward, targetDirection, singleStep, 0.0f);
                // Calculate a rotation a step closer to the target and applies rotation to this object
                enemy.transform.rotation = Quaternion.LookRotation(newDirection);
                if (enemy.transform.position != CurrentPositionHolder)
                {
                    enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, CurrentPositionHolder, Timer);
                }
                else
                {
                    if (CurrentNode < PathNode.Count - 1)
                    {
                        CurrentNode ++;
                        CheckNode();
                    }
                }
                if ((Player.transform.position.z - enemy.transform.position.z) >= 50)
                {
                    light1Comp.color = Color.green;
                    light2Comp.color = Color.green;
                    light3Comp.color = Color.green;
                    light3Comp.range = 1;
                }
                if ((Player.transform.position.z - enemy.transform.position.z) <= 50)
                {
                    light1Comp.color = Color.red;
                    light2Comp.color = Color.green;
                    light3Comp.color = Color.green;
                    light3Comp.range = 1;
                }
                if ((Player.transform.position.z - enemy.transform.position.z) <= 30)
                {
                    light1Comp.color = Color.red;
                    light2Comp.color = Color.red;
                    light3Comp.color = Color.green;
                    light3Comp.range = 1;
                }
                if ((Player.transform.position.z - enemy.transform.position.z) <= 10)
                {
                    light1Comp.color = Color.red;
                    light2Comp.color = Color.red;
                    light3Comp.color = Color.red;
                    light3Comp.range = 4;
                }
            }
        }
    }
}

