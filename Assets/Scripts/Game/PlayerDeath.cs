﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDeath : MonoBehaviour
{
    private int currentScore;
    private List<int> HighScores = new List<int>();
    public PathFollower PathFollower;
    public void Death()
    {
        Debug.Log("Death function");
        currentScore = Score.PlayerScore;
        //Score.LoadFromFile();
        //Debug.Log("Laded");
        //HighScores = Score.HighScores.ToList();
        //HighScores.Add(currentScore);
        //HighScores.Sort();
        //Debug.Log(HighScores);
        SceneManager.LoadScene("Menu");
    }
}
