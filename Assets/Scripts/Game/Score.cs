﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class SaveData
{
    public int[] HighScores;
}
public class Score : MonoBehaviour
{
    public GameObject Player;
    public Text ScoreText;

    static public int PlayerScore;
    static public int[] HighScores;
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        PlayerScore = Mathf.RoundToInt(Player.transform.position.z);
        ScoreText.text = PlayerScore + "m";
    }

    public static void SaveScoreToFile()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.dataPath + "save.adamshaircut";

        FileStream stream = new FileStream(path, FileMode.Create);
        formatter.Serialize(stream, ConvertToData());

        stream.Close();
    }
    static SaveData ConvertToData()
    {
        SaveData data = new SaveData
        {
            HighScores = new int[11]
        };
        return data;
    }

    public static void LoadFromFile()
    {
        string path = Application.dataPath + "save.adamshaircut";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();

            FileStream stream = new FileStream(path, FileMode.Open);

            ConvertFromData(formatter, stream);
            stream.Close();
        }
    }

    static void ConvertFromData(BinaryFormatter formatter, FileStream stream)
    {
        SaveData data = formatter.Deserialize(stream) as SaveData;

        HighScores = data.HighScores;
    }
}
