﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillPlane : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Player;
    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Player.transform.position.x, transform.position.y, Player.transform.position.z);
    }
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("Player collided with killplane");
            Player.GetComponent<PlayerDeath>().Death();
            SceneManager.LoadScene(0);
            PathFollower.CurrentPositionHolder = Vector3.zero;
            print("player is ded");
            
        }
    }
}
